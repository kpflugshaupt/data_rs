use std::ffi::OsStr;
use std::path::PathBuf;

use anyhow::{bail, Result};
use clap::{Parser, Subcommand};

mod commands;
mod data_file;
mod util;

#[derive(Parser, Debug)]
#[command(author, version, about="Data file tool", long_about = None)]
struct Cli {
    /// Turn debugging information on
    #[arg(short, long, value_parser)]
    debug: bool,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Convert a CSV data file to a Parquet file
    ToParquet {
        /// Path to CSV file
        #[arg(value_parser, value_name = "CSV_FILE")]
        csv_path: PathBuf,

        /// Force conversion for files without .csv extension [false]
        #[arg(short, long, value_parser, default_value_t = false)]
        force: bool,
    },
    /// Convert a Parquet data file to a CSV file
    ToCSV {
        /// Path to Parquet file
        #[arg(value_parser, value_name = "PARQUET_FILE")]
        parquet_path: PathBuf,

        /// Force conversion for files without .parquet extension [false]
        #[arg(short, long, value_parser, default_value_t = false)]
        force: bool,
    },
    /// Scan a data file (CSV or Parquet) and show its structure
    Schema {
        /// Path to data file
        #[arg(value_parser, value_name = "DATA_FILE")]
        file_path: PathBuf,
    },
    /// Sample a data file (CSV or Parquet)
    Sample {
        /// number of rows to sample
        #[arg(value_parser, short = 'n')]
        n_rows: usize,
        /// Path to data file
        #[arg(value_parser, value_name = "DATA_FILE")]
        file_path: PathBuf,
    },
    /// Convert a text file encoded in win-1252 to UTF8
    WinToUTF8 {
        /// Path to existing win-1252 text file
        #[arg(value_parser, value_name = "WIN_FILE")]
        win_path: PathBuf,

        /// Path to new UTF8 text file
        #[arg(value_parser, value_name = "UTF8_FILE")]
        utf8_path: PathBuf,
    },
    /// Convert a text file encoded in UTF16 LE to UTF8
    UTF16ToUTF8 {
        /// Path to existingUTF16 text file
        #[arg(value_parser, value_name = "UTF16_FILE")]
        utf16_path: PathBuf,

        /// Path to new UTF8 text file
        #[arg(value_parser, value_name = "UTF8_FILE")]
        utf8_path: PathBuf,
    },
}

fn main() -> Result<()> {
    let args = Cli::parse();
    if args.debug {
        println!("DEBUG Arguments: {:#?}", args);
    }
    match &args.command {
        Commands::ToParquet { csv_path, force } => {
            if csv_path.extension().unwrap_or_else(|| OsStr::new("")) != "csv" && !(*force) {
                bail!("File extension is not '.csv'! Use --force parameter to convert anyway.")
            }
            commands::csv_to_parquet(csv_path, args.debug)?;
        }
        Commands::ToCSV {
            parquet_path,
            force,
        } => {
            if parquet_path.extension().unwrap_or_else(|| OsStr::new("")) != "parquet" && !(*force)
            {
                bail!("File extension is not '.parquet'! Use --force parameter to convert anyway.")
            }
            commands::parquet_to_csv(parquet_path, args.debug)?;
        }
        Commands::Schema { ref file_path } => {
            let output = commands::schema(file_path)?;
            println!("Scanned file {:?}", file_path);
            for line in output.lines() {
                println!("{}", line);
            }
        }
        Commands::Sample { n_rows, file_path } => {
            let output = commands::sample(file_path, n_rows)?;
            println!("Sampled {} rows from file {:?}", n_rows, file_path);
            for line in output.lines() {
                println!("{}", line);
            }
        }
        Commands::WinToUTF8 {
            win_path,
            utf8_path,
        } => {
            commands::win_to_utf8(win_path, utf8_path)?;
        }
        Commands::UTF16ToUTF8 {
            utf16_path,
            utf8_path,
        } => {
            commands::utf16_to_utf8(utf16_path, utf8_path)?;
        }
    }
    Ok(())
}
