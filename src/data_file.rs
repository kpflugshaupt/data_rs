use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::Path;

use anyhow::{bail, Context, Result};
use encoding::all::{UTF_16LE, WINDOWS_1252};
use encoding::{DecoderTrap, Encoding};
use polars::prelude::{
    CsvReader, CsvWriter, DataFrame, LazyCsvReader, LazyFileListReader, LazyFrame,
    ParquetCompression, ParquetReader, ParquetWriter, ScanArgsParquet, SerReader, SerWriter,
};

fn convert_file_to_utf8(source_fmt: &str, file_path: &Path, new_file_path: &Path) -> Result<()> {
    // build a decoding function
    let decode = match source_fmt {
        "WINDOWS_1252" => |line: &Vec<u8>| WINDOWS_1252.decode(line, DecoderTrap::Strict),
        "UTF16_LE" => |line: &Vec<u8>| UTF_16LE.decode(line, DecoderTrap::Strict),
        _ => bail!(format!(
            r#"source_fmt must be "WINDOWS_1252" or "UTF16_LE" [{:?}]"#,
            source_fmt
        )),
    };
    let file =
        File::open(file_path).with_context(|| format!("could not open file {:?}", file_path))?;
    let reader = BufReader::new(&file);
    let new_file = File::create(new_file_path)
        .with_context(|| format!("could not create file {:?}", new_file_path))?;
    let mut writer = BufWriter::new(&new_file);

    // decode each line of input file, write it to output file
    for line in reader.split(b'\n').map(|line| line.unwrap()) {
        let mut decoded =
            decode(&line).expect(&format!("error converting file from {}", source_fmt));
        decoded += "\n";
        writer.write(decoded.as_ref()).with_context(|| {
            format!(
                "error writing string '{:?}' to target file {:?}",
                &decoded, &new_file_path
            )
        })?;
    }
    Ok(())
}

pub fn win_to_utf8(file_path: &Path, new_file_path: &Path) -> Result<()> {
    convert_file_to_utf8("WINDOWS_1252", file_path, new_file_path)?;
    Ok(())
}

pub fn utf16_to_utf8(file_path: &Path, new_file_path: &Path) -> Result<()> {
    convert_file_to_utf8("UTF16_LE", file_path, new_file_path)?;
    Ok(())
}

pub fn load_csv_file(file_path: &Path) -> Result<DataFrame> {
    CsvReader::from_path(file_path)
        .with_context(|| format!("could not open CSV file {:?}", file_path))?
        .has_header(true)
        .with_separator(b',')
        .with_quote_char(Some(b'"'))
        .finish()
        .with_context(|| format!("could not load CSV file {:?}", file_path))
}

pub fn scan_csv_file(file_path: &Path) -> Result<LazyFrame> {
    LazyCsvReader::new(file_path.to_string_lossy().into_owned())
        .has_header(true)
        .with_separator(b',')
        .with_quote_char(Some(b'"'))
        .with_try_parse_dates(true)
        .finish()
        .with_context(|| format!("could not scan CSV file {:?}", file_path))
}

pub fn save_csv_file(file_path: &Path, data: &mut DataFrame) -> Result<()> {
    let csv_file = File::create(file_path)
        .with_context(|| format!("Could not create CSV file {:?}", file_path))?;
    CsvWriter::new(csv_file)
        .include_header(true)
        .with_separator(b',')
        .with_quote_char(b'"')
        .finish(data)
        .unwrap_or_else(|_| panic!("Failed writing dataframe to file {:?}", file_path));
    Ok(())
}

pub fn load_parquet_file(file_path: &Path) -> Result<DataFrame> {
    let parquet_file = File::open(file_path)
        .with_context(|| format!("could not open Parquet file {:?}", file_path))?;
    ParquetReader::new(parquet_file)
        .finish()
        .with_context(|| format!("could not load Parquet file {:?}", file_path))
}

pub fn scan_parquet_file(file_path: &Path) -> Result<LazyFrame> {
    LazyFrame::scan_parquet(
        file_path.to_string_lossy().into_owned(),
        ScanArgsParquet::default(),
    )
    .with_context(|| format!("could not scan Parquet file {:?}", file_path))
}

pub fn save_parquet_file(file_path: &Path, data: &mut DataFrame) -> Result<()> {
    let parquet_file = File::create(file_path)
        .with_context(|| format!("Could not create parquet file {:?}", file_path))?;
    ParquetWriter::new(parquet_file)
        .with_compression(ParquetCompression::Snappy)
        .finish(data)
        .unwrap_or_else(|_| panic!("Failed writing dataframe to file {:?}", file_path));
    Ok(())
}

pub fn load_data_file(file_path: &Path) -> Result<DataFrame> {
    let ext = file_path
        .extension()
        .expect("Cannot get data file extension");
    match ext.to_str() {
        Some("csv") => load_csv_file(file_path),
        Some("parquet") => load_parquet_file(file_path),
        Some(&_) | None => bail!(format!(
            r#"File extension must be "csv" or "parquet" [{:?}]"#,
            ext
        )),
    }
}

pub fn scan_data_file(file_path: &Path) -> Result<LazyFrame> {
    let ext = file_path
        .extension()
        .expect("Cannot get data file extension");
    match ext.to_str() {
        Some("csv") => scan_csv_file(file_path),
        Some("parquet") => scan_parquet_file(file_path),
        Some(&_) | None => bail!(format!(
            r#"File extension must be "csv" or "parquet" [{:?}]"#,
            ext
        )),
    }
}

#[cfg(test)]
mod data_file_tests {
    use crate::commands::win_to_utf8;
    use std::io::Read;
    use std::{assert_eq, fs};

    use super::*;

    #[test]
    fn fail_on_non_utf8_text() {
        let csv_path = Path::new("test_data/test_data_windows-1252.csv");
        let result = load_csv_file(&csv_path);
        assert!(result.is_err());

        let error = result.err().unwrap();
        assert_eq!(
            error.to_string(),
            r#"could not load CSV file "test_data/test_data_windows-1252.csv""#
        );
        assert_eq!(
            error.source().unwrap().to_string(),
            "invalid utf-8 sequence in csv"
        )
    }

    #[test]
    fn full_round_trip() {
        // prepare file paths
        let csv_path = Path::new("test_data/round_trip_test_data.csv");
        let parquet_path = csv_path.with_extension("parquet");
        assert!(csv_path.is_file());
        fs::remove_file(&parquet_path).unwrap_or_default();

        // load CSV file
        let result = load_csv_file(&csv_path);
        assert!(result.is_ok());
        let mut dataframe = result.unwrap();

        // write to Parquet file
        let result = save_parquet_file(&parquet_path, &mut dataframe);
        assert!(result.is_ok());
        assert!(parquet_path.is_file());

        // reload Parquet file, compare new data to initial data
        let result = load_parquet_file(&parquet_path);
        assert!(result.is_ok());
        let mut new_dataframe = result.unwrap();
        assert_eq!(new_dataframe, dataframe);

        // save new data to new CSV file
        let csv_path_new = csv_path.with_extension("csv_new");
        let result = save_csv_file(&csv_path_new, &mut new_dataframe);
        assert!(result.is_ok());
        assert!(csv_path_new.is_file());

        // load new CSV file, compare data again
        let result = load_csv_file(&csv_path_new);
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), dataframe);

        // remove result files
        assert!(fs::remove_file(&parquet_path).is_ok());
        assert!(fs::remove_file(&csv_path_new).is_ok());
    }

    #[test]
    fn convert_win_1252() {
        // convert file to new file, ensure result
        let csv_path = Path::new("test_data/test_data_windows-1252.csv");
        let new_csv_path = csv_path.with_extension("csv_utf8");
        let result = win_to_utf8(&csv_path, &new_csv_path);
        assert!(result.is_ok());
        assert!(new_csv_path.is_file());

        // compare contents of new and old file
        let mut old_file = File::open(csv_path).unwrap();
        let mut old_content = Vec::new();
        old_file.read_to_end(&mut old_content).unwrap();
        let mut new_file = File::open(&new_csv_path).unwrap();
        let mut new_content = Vec::new();
        new_file.read_to_end(&mut new_content).unwrap();
        assert_eq!(
            String::from_utf8(new_content).unwrap(),
            WINDOWS_1252
                .decode(old_content.as_slice(), DecoderTrap::Strict)
                .unwrap()
        );

        // remove new file
        assert!(fs::remove_file(&new_csv_path).is_ok());
    }
}
