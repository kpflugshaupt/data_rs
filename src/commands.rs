use std::path::Path;

use std::cmp::min;

use anyhow::Result;

use crate::data_file;

/// Parse a CSV file and write its contents to a Parquet file,
/// replacing .csv extension by .parquet.
pub fn csv_to_parquet(csv_path: &Path, debug: bool) -> Result<()> {
    if debug {
        println!("DEBUG: convert CSV file {:?}", csv_path)
    }
    let mut dataframe = data_file::load_csv_file(csv_path)?;
    let parquet_path = csv_path.with_extension("parquet");
    if debug {
        println!("DEBUG: Writing dataframe to file {:?}", parquet_path)
    }
    data_file::save_parquet_file(&parquet_path, &mut dataframe)?;
    Ok(())
}

/// Parse a Parquet file and write its contents to a CSV file,
/// replacing .parquet extension by .csv.
pub fn parquet_to_csv(parquet_path: &Path, debug: bool) -> Result<()> {
    if debug {
        println!("DEBUG: convert Parquet file {:?}", parquet_path)
    }
    let mut dataframe = data_file::load_parquet_file(parquet_path)?;
    let csv_path = parquet_path.with_extension("csv");
    if debug {
        println!("DEBUG: Writing dataframe to file {:?}", csv_path)
    }
    data_file::save_csv_file(&csv_path, &mut dataframe)?;
    Ok(())
}

pub fn schema(file_path: &Path) -> Result<String> {
    let lazy_df = data_file::scan_data_file(file_path)?;
    Ok(format!("{:?}", lazy_df.schema().unwrap()))
}

pub fn sample(file_path: &Path, n_rows: &usize) -> Result<String> {
    let df = data_file::load_data_file(file_path)?;
    let rows_to_sample = min(*n_rows, df.height());
    let sample_n_rows = df
        .sample_n_literal(rows_to_sample, false, false, None)
        .unwrap();
    Ok(format!("{}", sample_n_rows))
}

pub fn win_to_utf8(win_path: &Path, utf8_path: &Path) -> Result<()> {
    data_file::win_to_utf8(win_path, utf8_path)
}

pub fn utf16_to_utf8(utf16_path: &Path, utf8_path: &Path) -> Result<()> {
    data_file::utf16_to_utf8(utf16_path, utf8_path)
}

#[cfg(test)]
mod command_tests {
    use std::fs;

    use crate::util;
    use polars::prelude::DataType;

    use super::*;

    fn error_source_text(error: anyhow::Error) -> String {
        error.source().unwrap().to_string()
    }

    #[test]
    fn sample_test_file() {
        let csv_path = Path::new("test_data/test_data.csv");
        let result = sample(&csv_path, &1);
        assert!(result.is_ok());
        assert!(result.unwrap().starts_with("shape: (1, 4)"));
    }

    #[test]
    fn csv_schema_fails_on_non_existent_file() {
        let non_existent_path = Path::new("this_path_does_not_exist.csv");
        let result = schema(&non_existent_path);
        assert!(result.is_err());

        let error = result.err().unwrap();
        assert_eq!(
            error.to_string(),
            r#"could not scan CSV file "this_path_does_not_exist.csv""#
        );
        let source_text = error_source_text(error);
        assert_eq!(&source_text[0..12], "No such file")
    }

    #[test]
    fn convert_fails_on_non_existent_file() {
        let non_existent_path = Path::new("this_path_does_not_exist.csv");
        let result = csv_to_parquet(&non_existent_path, false);
        assert!(result.is_err());

        let error = result.err().unwrap();
        assert_eq!(
            error.to_string(),
            r#"could not open CSV file "this_path_does_not_exist.csv""#
        );
        let source_text = error_source_text(error);
        assert_eq!(&source_text[0..12], "No such file")
    }

    #[test]
    fn csv_schema_works_on_valid_file() {
        let csv_path = Path::new("test_data/test_data.csv");
        let result = schema(&csv_path);
        assert!(result.is_ok());

        let schema = result.unwrap();
        let correct_schema = [
            "Schema:",
            "name: Text, data type: String",
            "name: Number, data type: Int64",
            "name: Flag, data type: Boolean",
            "name: Date, data type: Date",
        ]
        .join("\n");
        assert_eq!(schema.trim(), correct_schema);
    }

    #[test]
    fn convert_works_on_valid_file() {
        let csv_path = Path::new("test_data/test_data.csv");
        let result = csv_to_parquet(&csv_path, false);
        assert!(result.is_ok());

        let parquet_path = csv_path.with_extension("parquet");
        assert!(parquet_path.is_file());

        let data_frame = data_file::load_parquet_file(&parquet_path).unwrap();
        assert_eq!(data_frame.shape(), (3, 4));
        assert_eq!(
            data_frame.get_column_names(),
            &["Text", "Number", "Flag", "Date"]
        );
        assert_eq!(
            data_frame.dtypes(),
            &[
                DataType::String,
                DataType::Int64,
                DataType::Boolean,
                DataType::Date
            ]
        );
        assert!(fs::remove_file(parquet_path).is_ok());
    }
}
